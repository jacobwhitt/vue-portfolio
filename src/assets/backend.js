export default [
  { name: 'express.js', src: require('../assets/backend-photos/express.png') },
  { name: 'node.js', src: require('../assets/backend-photos/nodejs.png') },
  { name: 'graphql', src: require('../assets/backend-photos/graphql.png') },
  { name: 'mongodb', src: require('../assets/deployment-photos/mongodb.png') },
  { name: 'aws-amplify', src: require('../assets/deployment-photos/amplify.png') },
  { name: 'firebase', src: require('../assets/deployment-photos/firebase.png') },
];