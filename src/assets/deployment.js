export default [
  { name: 'npm', src: require('../assets/deployment-photos/npm.png') },
  { name: 'git', src: require('../assets/deployment-photos/git.png') },
  { name: 'gitlab', src: require('../assets/deployment-photos/gitlab.png') },
  { name: 'digitalocean', src: require('../assets/deployment-photos/digitalocean.png') },
];