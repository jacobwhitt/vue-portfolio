export default [
  { name: 'js es6', src: require('../assets/frontend-photos/js.jpg') },
  { name: 'typescript', src: require('../assets/frontend-photos/typescript.png') },
  { name: 'html 5', src: require('../assets/frontend-photos/html.png') },
  { name: 'css 3', src: require('../assets/frontend-photos/css.png') },
  { name: 'bootstrap 3', src: require('../assets/frontend-photos/bootstrap.png') },
  { name: 'bulma', src: require('../assets/frontend-photos/bulma.png') },
  { name: 'react', src: require('../assets/frontend-photos/react.png') },
  { name: 'reactnative', src: require('../assets/frontend-photos/reactnative.jpg') },
  { name: 'redux', src: require('../assets/frontend-photos/redux.png') },
  { name: 'vue 3', src: require('../assets/frontend-photos/vue.png') },
  { name: 'nuxt', src: require('../assets/frontend-photos/nuxt.png') },

];