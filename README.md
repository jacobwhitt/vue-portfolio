# Jacob Whitt's Vue 3 Digital Portfolio

## Project setup
```
npm install
```
>Install all dependencies to run the project locally

### Compiles and hot-reloads for development
```
npm run serve
```
>This will run a local version of the code at http://localhost:8080

### Compiles and minifies for production
```
npm run build
```